package applicant;

import java.util.function.Predicate;

public class CriminalRecordsEvaluator extends EvaluatorChain {

    public CriminalRecordsEvaluator(Evaluator next) {
        super(next);
    }

    public Predicate<Applicant> evaluate(Applicant applicant) {
        return theApplicant -> !theApplicant.hasCriminalRecord();
    }
}
