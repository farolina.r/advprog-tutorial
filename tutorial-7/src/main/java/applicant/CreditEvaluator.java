package applicant;

import java.util.function.Predicate;

public class CreditEvaluator extends EvaluatorChain {

    public CreditEvaluator(Evaluator next) {
        super(next);
    }

    public Predicate<Applicant> evaluate(Applicant applicant) {
        return theApplicant -> theApplicant.getCreditScore() > 600;
    }
}