package applicant;

import java.util.function.Predicate;

public class QualifiedEvaluator implements Evaluator {

    public Predicate<Applicant> evaluate(Applicant applicant) {
        return theApplicant -> theApplicant.isCredible();
    }
}
