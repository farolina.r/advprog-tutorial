import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ScoreGroupingTest {
    // TODO Implement me!
    // Increase code coverage in ScoreGrouping class
    // by creating unit test(s)!
    private static Map<String, Integer> scores = new HashMap<>();

    @Before
    public void setUp(){
        scores.put("Alice", 12);
        scores.put("Bob", 15);
        scores.put("Charlie", 11);
        scores.put("Delta", 15);
        scores.put("Emi", 15);
        scores.put("Foxtrot", 11);
    }

    @Test
    public void testGroupingByScores(){
        Map<Integer, List<String>> byScores = new HashMap<>();
        byScores = ScoreGrouping.groupByScores(scores);
        assertEquals(3, byScores.get(15).size());
        assertEquals(2, byScores.get(11).size());
        assertEquals(1, byScores.get(12).size());
    }
}
