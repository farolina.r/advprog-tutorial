package applicant;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ApplicantTest {
    // TODO Implement me!
    // Increase code coverage in Applicant class
    // by creating unit test(s)!

    private static Applicant applicant;
    private static boolean result;

    @Before
    public void setUp(){
        applicant = new Applicant();
    }

    @Test
    public void testCreditQualifiedApplicant(){
        result = applicant.evaluate(applicant, new CreditEvaluator(new QualifiedEvaluator()));
        assertTrue(result);
    }

    @Test
    public void testCreditEmploymentQualifiedApplicant(){
        result = applicant.evaluate(applicant,
                new CreditEvaluator(new EmploymentEvaluator(new QualifiedEvaluator())));
        assertTrue(result);
    }

    @Test
    public void testCriminalRecordEmploymentQualifiedApplicant(){
        result = applicant.evaluate(applicant,
                new CriminalRecordsEvaluator(
                        new EmploymentEvaluator(new QualifiedEvaluator())));
        assertFalse(result);
    }

    @Test
    public void testCriminalRecordCreditEmploymentQualifiedApplicant(){
        result = applicant.evaluate(applicant,
                new CriminalRecordsEvaluator(
                        new CreditEvaluator(
                                new EmploymentEvaluator(new QualifiedEvaluator()))));
        assertFalse(result);
    }

}
