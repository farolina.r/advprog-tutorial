package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class SecurityExpert extends Employees{
	public SecurityExpert(String nama, double salary) {
		this.name = nama;
		this.salary = salary;
		this.role = "Security Expert";
		// TODO Auto-generated constructor stub
	}

	@Override
	public double getSalary() {
		// TODO Auto-generated method stub
		return salary;
	}
}
