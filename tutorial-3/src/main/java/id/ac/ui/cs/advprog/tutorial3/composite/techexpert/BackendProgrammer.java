package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class BackendProgrammer extends Employees {
	
	public BackendProgrammer(String nama, double salary) {
		this.name = nama;
		this.salary = salary;
		this.role = "Back End Programmer";
		// TODO Auto-generated constructor stub
	}

	@Override
	public double getSalary() {
		// TODO Auto-generated method stub
		return salary;
	}
    //TODO Implement
}
