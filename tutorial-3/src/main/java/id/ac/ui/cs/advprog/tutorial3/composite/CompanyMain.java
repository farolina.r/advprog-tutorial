package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.*;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.*;

public class CompanyMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Company comp = new Company();
		
		Employees ceo = new Ceo("Glace", 150000.0);
		Employees cto = new Cto("Jaden", 100000.0);
		Employees bep = new BackendProgrammer("Cybele", 60000.0);
		Employees fep = new FrontendProgrammer("Jekyll", 40000.0);
		Employees net = new NetworkExpert("Matthew", 70000.0);
		Employees sec = new SecurityExpert("Blue", 80000.0);
		
		comp.addEmployee(ceo);
		comp.addEmployee(cto);
		comp.addEmployee(bep);
		comp.addEmployee(fep);
		comp.addEmployee(net);
		comp.addEmployee(sec);
		
		comp.getAllEmployees();
		System.out.println("Net salary: " + comp.getNetSalaries());
	}

}
