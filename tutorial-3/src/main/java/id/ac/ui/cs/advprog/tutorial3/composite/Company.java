package id.ac.ui.cs.advprog.tutorial3.composite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Company {
    protected List<Employees> employeesList;

    public Company() {
        employeesList = new ArrayList<Employees>();
    }

    public Company(List<Employees> employeesList) {
        Collections.copy(this.employeesList, employeesList);
    }

    public void addEmployee(Employees employees) {
        //TODO Implement
    	employeesList.add(employees);
    }
    
   
    public double getNetSalaries() {
        //TODO Implement
    	Iterator<Employees> iterator = employeesList.iterator();
    	double net = 0;
    	while (iterator.hasNext()) {
			Employees employees = (Employees)iterator.next();
			net += employees.getSalary();
		}
    	return net;
    }

    public List<Employees> getAllEmployees() {
        //TODO Implement
    	/*Iterator<Employees> iterator = employeesList.iterator();
    	*/
    	Iterator<Employees> iterator = employeesList.iterator();
    	while (iterator.hasNext()) {
			Employees employees = (Employees)iterator.next();
			System.out.println(employees.getName() + ", $" + employees.getSalary());
		}
    	return employeesList;
    }
}
