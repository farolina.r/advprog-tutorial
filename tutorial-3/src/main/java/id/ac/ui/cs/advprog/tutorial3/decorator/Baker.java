package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class Baker {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Food thinbun = BreadProducer.THIN_BUN.createBreadToBeFilled();
		System.out.println(thinbun.getDescription() + " $" + thinbun.cost());
		
		Food thickbun = BreadProducer.THICK_BUN.createBreadToBeFilled();
		thickbun = FillingDecorator.CHEESE.addFillingToBread(thickbun);
		thickbun = FillingDecorator.CHEESE.addFillingToBread(thickbun);
		thickbun = FillingDecorator.CHICKEN_MEAT.addFillingToBread(thickbun);
		System.out.println(thickbun.getDescription() + " $" + thickbun.cost());
		
		Food crustsandwich = BreadProducer.CRUSTY_SANDWICH.createBreadToBeFilled();
		crustsandwich = FillingDecorator.BEEF_MEAT.addFillingToBread(crustsandwich);
		crustsandwich = FillingDecorator.CHILI_SAUCE.addFillingToBread(crustsandwich);
		crustsandwich = FillingDecorator.CUCUMBER.addFillingToBread(crustsandwich);
		System.out.println(crustsandwich.getDescription() + " $" + crustsandwich.cost());
	}

}
