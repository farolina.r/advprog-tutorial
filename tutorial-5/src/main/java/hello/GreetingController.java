package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name = "visitor", required = false)
                                       String visitor, Model model) {
        if(visitor == null || visitor.equals("")){
            model.addAttribute("visitor", "This is My CV.");
        }
        else{
            model.addAttribute("visitor", visitor + ", I hope you interested to hire me.");
        }
        String myname = "Farolina Rahmatunnisa";
        model.addAttribute("myname", myname);

        String birthdate = "May 22, 1999";
        model.addAttribute("birthdate", birthdate);

        String birthplace = "Tangerang";
        model.addAttribute("birthplace", birthplace);

        String address = "Sentul City";
        model.addAttribute("address", address);

        String elementary = "Taruna Bangsa";
        model.addAttribute("elementary", elementary);

        String midschool = "SMPN 4 Bogor";
        model.addAttribute("midschool", midschool);

        String highschool = "SMAN 3 Bogor";
        model.addAttribute("highschool", highschool);

        String description = "I love coffee";
        model.addAttribute("description", description);

        return "greeting";
    }

}
