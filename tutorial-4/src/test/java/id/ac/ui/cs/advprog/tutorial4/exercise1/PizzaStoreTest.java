package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

public class PizzaStoreTest {
	private Pizza pizza;
	private PizzaStore newYorkPizzaStore;
	
	@Before
    public void setUp() {
        newYorkPizzaStore = new NewYorkPizzaStore();
    }
	
	@Test
    public void testMethodOrderPizza() {
		pizza = newYorkPizzaStore.orderPizza("cheese");
		assertTrue(pizza instanceof CheesePizza);
        //assertEquals("--- Making a New York Style Cheese Pizza ---", "--- Making a " + pizza.getName() + " ---");
        //assertEquals("Preparing " + pizza.getName(), pizza.prepare());
        //assertEquals("Bake for 25 minutes at 350", pizza.bake());
        //assertEquals("Cutting the pizza into diagonal slices", pizza.cut());
        //assertEquals("Place pizza in official PizzaStore box", pizza.box());
    }
}
