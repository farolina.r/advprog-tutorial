package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

public class DepokPizzaStoreTest {
	private Pizza pizza;
	private PizzaStore depokPizzaStore;
	
	@Before
    public void setUp() {
        depokPizzaStore = new DepokPizzaStore();
    }
	
	@Test
    public void testItemCheese() {
		pizza = depokPizzaStore.createPizza("cheese");
        assertEquals("Depok Style Cheese Pizza", pizza.getName());
    }
	
	@Test
    public void testItemVeggie() {
		pizza = depokPizzaStore.createPizza("veggie");
        assertEquals("Depok Style Veggie Pizza", pizza.getName());
    }
	
	@Test
    public void testItemClam() {
		pizza = depokPizzaStore.createPizza("clam");
        assertEquals("Depok Style Clam Pizza", pizza.getName());
    }
}
