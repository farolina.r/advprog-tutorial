package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

public class CheeseTest {
	private Cheese camembertCheese;
	private Cheese mozzarellaCheese;
	private Cheese parmesanCheese;
	private Cheese reggianoCheese;
	
	
	@Before
    public void setUp() {
		camembertCheese = new CamembertCheese();
		mozzarellaCheese = new MozzarellaCheese();
		parmesanCheese = new ParmesanCheese();
		reggianoCheese = new ReggianoCheese();
    }
	
	@Test
    public void testCamembertCheese() {
        assertEquals(camembertCheese.toString(), "Shredded Camembert");
    }
	
	@Test
    public void testMozzarellaCheese() {
        assertEquals(mozzarellaCheese.toString(), "Shredded Mozzarella");
    }
	
	@Test
    public void testParmesanCheese() {
        assertEquals(parmesanCheese.toString(), "Shredded Parmesan");
    }
	
	@Test
    public void testReggianoCheese() {
        assertEquals(reggianoCheese.toString(), "Reggiano Cheese");
    }
	
}
