package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.DepokPizzaStore;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.NewYorkPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.PizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese;

public class ClamPizzaTest {
	private PizzaIngredientFactory newYorkPizzaIngredientFactory;
	private Pizza clamPizza;
	
	@Before
    public void setUp() {
        newYorkPizzaIngredientFactory = new NewYorkPizzaIngredientFactory();
        clamPizza = new ClamPizza(newYorkPizzaIngredientFactory);
    }
	
	@Test
	public void testPrepare(){
		clamPizza.prepare();
		assertTrue(clamPizza.dough instanceof ThinCrustDough);
		assertTrue(clamPizza.sauce instanceof MarinaraSauce);
		assertTrue(clamPizza.cheese instanceof ReggianoCheese);
	}
}
