package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

public class SauceTest {
	private Sauce bechamelSauce;
	private Sauce marinaraSauce;
	private Sauce plumTomatoSauce;
	
	
	@Before
    public void setUp() {
		bechamelSauce = new BechamelSauce();
		marinaraSauce = new MarinaraSauce();
		plumTomatoSauce = new PlumTomatoSauce();
    }
	
	@Test
    public void testBechamelSauce() {
        assertEquals(bechamelSauce.toString(), "Bechamel Sauce for White Pizza");
    }
	
	@Test
    public void testMarinaraSauce() {
        assertEquals(marinaraSauce.toString(), "Marinara Sauce");
    }
	
	@Test
    public void testPlumTomatoSauce() {
        assertEquals(plumTomatoSauce.toString(), "Tomato sauce with plum tomatoes");
    }
	
}