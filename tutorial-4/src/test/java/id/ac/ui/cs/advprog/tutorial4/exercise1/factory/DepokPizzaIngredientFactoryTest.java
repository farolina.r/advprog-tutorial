package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.CheeseBurstCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.BechamelSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.CamembertCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackOlives;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Jalapeno;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Eggplant;;

public class DepokPizzaIngredientFactoryTest {
	private PizzaIngredientFactory depokPizzaIngredientFactory;
	
	@Before
    public void setUp() {
        depokPizzaIngredientFactory = new DepokPizzaIngredientFactory();
    }
	
	@Test
    public void testMethodCreateDough() {
        assertTrue(depokPizzaIngredientFactory.createDough() instanceof CheeseBurstCrustDough);
    }
	
	@Test
    public void testMethodCreateSauce() {
        assertTrue(depokPizzaIngredientFactory.createSauce() instanceof BechamelSauce);
    }
	
	@Test
    public void testMethodCreateCheese() {
        assertTrue(depokPizzaIngredientFactory.createCheese() instanceof CamembertCheese);
    }
	
	@Test
    public void testMethodCreateVeggies() {
        assertTrue(depokPizzaIngredientFactory.createVeggies()[0] instanceof BlackOlives);
        assertTrue(depokPizzaIngredientFactory.createVeggies()[1] instanceof Spinach);
        assertTrue(depokPizzaIngredientFactory.createVeggies()[2] instanceof Jalapeno);
        assertTrue(depokPizzaIngredientFactory.createVeggies()[3] instanceof Eggplant);
    }
}
