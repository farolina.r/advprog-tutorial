package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.DepokPizzaStore;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.NewYorkPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.PizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese;

public class VeggiePizzaTest {
	private PizzaIngredientFactory newYorkPizzaIngredientFactory;
	private Pizza veggiesPizza;
	
	@Before
    public void setUp() {
        newYorkPizzaIngredientFactory = new NewYorkPizzaIngredientFactory();
        veggiesPizza = new VeggiePizza(newYorkPizzaIngredientFactory);
    }
	
	@Test
	public void testPrepare(){
		veggiesPizza.prepare();
		assertTrue(veggiesPizza.dough instanceof ThinCrustDough);
		assertTrue(veggiesPizza.sauce instanceof MarinaraSauce);
		assertTrue(veggiesPizza.cheese instanceof ReggianoCheese);
	}
}