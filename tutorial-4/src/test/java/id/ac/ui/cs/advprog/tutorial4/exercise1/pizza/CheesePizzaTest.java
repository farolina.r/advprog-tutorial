package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.DepokPizzaStore;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.NewYorkPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.PizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese;

public class CheesePizzaTest {
	private PizzaIngredientFactory newYorkPizzaIngredientFactory;
	private Pizza cheesePizza;
	
	@Before
    public void setUp() {
        newYorkPizzaIngredientFactory = new NewYorkPizzaIngredientFactory();
        cheesePizza = new CheesePizza(newYorkPizzaIngredientFactory);
    }
	
	@Test
	public void testPrepare(){
		cheesePizza.prepare();
		assertTrue(cheesePizza.dough instanceof ThinCrustDough);
		assertTrue(cheesePizza.sauce instanceof MarinaraSauce);
		assertTrue(cheesePizza.cheese instanceof ReggianoCheese);
	}
}
