package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

public class VeggiesTest {
	private Veggies blackOlives;
	private Veggies eggPlant;
	private Veggies garlic;
	private Veggies jalapeno;
	private Veggies mushroom;
	private Veggies onion;
	private Veggies redPepper;
	private Veggies spinach;
	
	
	
	@Before
    public void setUp() {
		blackOlives = new BlackOlives();
		eggPlant = new Eggplant();
		garlic = new Garlic();
		jalapeno = new Jalapeno();
		mushroom = new Mushroom();
		onion = new Onion();
		redPepper = new RedPepper();
		spinach = new Spinach();
    }
	
	@Test
    public void testBlackOlives() {
        assertEquals(blackOlives.toString(), "Black Olives");
    }
	
	@Test
    public void testEggplante() {
        assertEquals(eggPlant.toString(), "Eggplant");
    }
	
	@Test
    public void testGarlic() {
        assertEquals(garlic.toString(), "Garlic");
    }
	
	@Test
    public void testJalapeno() {
        assertEquals(jalapeno.toString(), "Jalapeno");
    }

	@Test
    public void testMushroom() {
        assertEquals(mushroom.toString(), "Mushrooms");
    }
	
	@Test
    public void testOnion() {
        assertEquals(onion.toString(), "Onion");
    }
	
	@Test
    public void testRedPepper() {
        assertEquals(redPepper.toString(), "Red Pepper");
    }
	
	@Test
    public void testSpinach() {
        assertEquals(spinach.toString(), "Spinach");
    }
	
}

