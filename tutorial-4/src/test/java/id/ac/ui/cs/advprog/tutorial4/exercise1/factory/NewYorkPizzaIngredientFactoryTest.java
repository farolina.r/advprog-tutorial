package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Garlic;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.RedPepper;

public class NewYorkPizzaIngredientFactoryTest {
	private PizzaIngredientFactory newYorkPizzaIngredientFactory;
	
	@Before
    public void setUp() {
		newYorkPizzaIngredientFactory = new NewYorkPizzaIngredientFactory();
    }
	
	@Test
    public void testMethodCreateDough() {
        assertTrue(newYorkPizzaIngredientFactory.createDough() instanceof ThinCrustDough);
    }
	
	@Test
    public void testMethodCreateSauce() {
        assertTrue(newYorkPizzaIngredientFactory.createSauce() instanceof MarinaraSauce);
    }
	
	@Test
    public void testMethodCreateCheese() {
        assertTrue(newYorkPizzaIngredientFactory.createCheese() instanceof ReggianoCheese);
    }
	
	@Test
    public void testMethodCreateVeggies() {
        assertTrue(newYorkPizzaIngredientFactory.createVeggies()[0] instanceof Garlic);
        assertTrue(newYorkPizzaIngredientFactory.createVeggies()[1] instanceof Onion);
        assertTrue(newYorkPizzaIngredientFactory.createVeggies()[2] instanceof Mushroom);
        assertTrue(newYorkPizzaIngredientFactory.createVeggies()[3] instanceof RedPepper);
    }
}
