package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

public class DoughTest {
	private Dough cheeseBurstCrust;
	private Dough thinCrust;
	private Dough thickCrust;
	
	
	@Before
    public void setUp() {
		cheeseBurstCrust = new CheeseBurstCrustDough();
		thinCrust = new ThinCrustDough();
		thickCrust = new ThickCrustDough();
    }
	
	@Test
    public void testCheeseBurstCrustDough() {
        assertEquals(cheeseBurstCrust.toString(), "Cheese Burst Crust Cheese Explosion dough");
    }
	
	@Test
    public void testThinCrustDough() {
        assertEquals(thinCrust.toString(), "Thin Crust Dough");
    }
	
	@Test
    public void testThickCrustDough() {
        assertEquals(thickCrust.toString(), "ThickCrust style extra thick crust dough");
    }
	
}

