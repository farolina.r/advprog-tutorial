package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.NewYorkPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.PizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class PizzaTest {
	private Pizza cheesePizza;
	private PizzaIngredientFactory newYorkIngredient;
	
	@Before
    public void setUp() {
        newYorkIngredient = new NewYorkPizzaIngredientFactory();
        cheesePizza = new CheesePizza(newYorkIngredient);
    }
	
	@Test
	public void testPrepare(){
		cheesePizza.prepare();
		assertTrue(cheesePizza.dough instanceof ThinCrustDough);
		assertTrue(cheesePizza.sauce instanceof MarinaraSauce);
		assertTrue(cheesePizza.cheese instanceof ReggianoCheese);
	}
	
	@Test
	public void testToString(){
		cheesePizza.prepare();
		String str1 = cheesePizza.getName();
		String strMain = cheesePizza.toString();
		String str2 = cheesePizza.dough.toString();
		String str3 = cheesePizza.sauce.toString();
		String str4 = cheesePizza.cheese.toString();
		
		//assertTrue(strMain.toLowerCase().contains(str1.toLowerCase()));
		//assertTrue(strMain.toLowerCase().contains(str2.toLowerCase()));
		//assertTrue(strMain.toLowerCase().contains(str3.toLowerCase()));
		//assertTrue(strMain.toLowerCase().contains(str4.toLowerCase()));
		assertEquals("---- " + str1 + " ----\n"+ str2 + "\n"
				+ str3 + "\n" + str4 + "\n", strMain);
		
	}
}