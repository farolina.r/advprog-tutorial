package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;


import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

public class NewYorkPizzaStoreTest {

		private Pizza pizza;
		private PizzaStore newYorkPizzaStore;
		
		@Before
	    public void setUp() {
	        newYorkPizzaStore = new NewYorkPizzaStore();
	    }
		
		@Test
	    public void testItemCheese() {
			pizza = newYorkPizzaStore.createPizza("cheese");
	        assertEquals("New York Style Cheese Pizza", pizza.getName());
	    }
		
		@Test
	    public void testItemVeggie() {
			pizza = newYorkPizzaStore.createPizza("veggie");
	        assertEquals("New York Style Veggie Pizza", pizza.getName());
	    }
		
		@Test
	    public void testItemClam() {
			pizza = newYorkPizzaStore.createPizza("clam");
	        assertEquals("New York Style Clam Pizza", pizza.getName());
	    }
	}
