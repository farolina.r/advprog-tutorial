package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class WhiteClams implements Clams{
	public String toString() {
        return "White Clams from Arcadia Bay";
    }
}
