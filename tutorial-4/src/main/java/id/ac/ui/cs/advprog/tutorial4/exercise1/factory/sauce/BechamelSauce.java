package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class BechamelSauce implements Sauce{
	public String toString() {
        return "Bechamel Sauce for White Pizza";
    }
}
