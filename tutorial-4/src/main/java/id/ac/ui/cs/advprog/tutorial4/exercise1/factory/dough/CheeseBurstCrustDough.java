package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class CheeseBurstCrustDough implements Dough{
	public String toString() {
        return "Cheese Burst Crust Cheese Explosion dough";
    }
}
